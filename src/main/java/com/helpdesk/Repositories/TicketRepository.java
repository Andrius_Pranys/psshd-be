package com.helpdesk.Repositories;

import com.helpdesk.Entities.TicketEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends JpaRepository<TicketEntity, Integer> {

    List<TicketEntity> findAll();

    TicketEntity findOneById(Integer id);

    TicketEntity save(TicketEntity ticketEntity);

    void deleteOneById(Integer id);

}
