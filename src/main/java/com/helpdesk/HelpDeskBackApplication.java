package com.helpdesk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class HelpDeskBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(HelpDeskBackApplication.class, args);
    }

}
