package com.helpdesk.Services;

import com.helpdesk.Converters.TicketConverter;
import com.helpdesk.Dto.Ticket;
import com.helpdesk.Entities.TicketEntity;
import com.helpdesk.Repositories.TicketRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TicketService {
    private TicketConverter ticketConverter;
    private TicketRepository ticketRepository;

    public TicketService(TicketConverter ticketConverter, TicketRepository ticketRepository) {
        this.ticketConverter = ticketConverter;
        this.ticketRepository = ticketRepository;
    }

    public List<Ticket> getAllTickets() {
        List<TicketEntity> ticketEntities = ticketRepository.findAll();
        return ticketConverter.convert(ticketEntities);
    }

    public Ticket getTicketById(Integer id) {
        TicketEntity ticketEntity = ticketRepository.findOneById(id);
        return ticketConverter.convert(ticketEntity);
    }

    public Ticket save(Ticket ticket) {
        TicketEntity ticketEntity = ticketConverter.convert(ticket);
        TicketEntity result = ticketRepository.save(ticketEntity);

        return ticketConverter.convert(result);
    }

    @Transactional
    public void delete(Integer id) {
        ticketRepository.deleteOneById(id);
    }
}
