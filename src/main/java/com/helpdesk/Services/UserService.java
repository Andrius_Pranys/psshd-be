package com.helpdesk.Services;

import com.helpdesk.Converters.UserConverter;
import com.helpdesk.Dto.User;
import com.helpdesk.Entities.UserEntity;
import com.helpdesk.Repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService{

    private UserRepository userRepository;
    private UserConverter userConverter;

    public UserService(UserRepository userRepository, UserConverter userConverter) {
        this.userRepository = userRepository;
        this.userConverter = userConverter;
    }

    public List<User> getAllUsers(){
        List<UserEntity> userEntities = userRepository.findAll();
        return userConverter.convert(userEntities);
    }
}
