package com.helpdesk.Entities;

import org.hibernate.engine.FetchStrategy;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user")
public class UserEntity extends AbstractEntity {

    @Column(name = "email", unique = true, nullable = false, length = 250)
    private String email;

    @Column(name = "password", nullable = false, length = 60)
    private String password;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @Column(name = "admin", nullable = false)
    private Boolean admin;

    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "userEntity", orphanRemoval = true)
    private List<TicketEntity> tickets = new ArrayList<>();

    public UserEntity() {
    }

    public List<TicketEntity> getTickets() {
        return tickets;
    }

    public void setTickets(List<TicketEntity> tickets) {
        this.tickets = tickets;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }
}
