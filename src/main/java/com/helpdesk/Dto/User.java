package com.helpdesk.Dto;

import com.helpdesk.Entities.TicketEntity;

import java.util.ArrayList;
import java.util.List;

public class User {

    private int id;
    private String email;
    private String password;
    private String name;
    private Boolean admin;

    private List<Ticket> tickets = new ArrayList<>();

    public User() {
    }

    public User(int id, String email, String name, Boolean admin, List<Ticket> tickets) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.admin = admin;
        this.tickets = tickets;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }
}
