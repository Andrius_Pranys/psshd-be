package com.helpdesk.Dto;

public class Ticket {

    private int id;
    private int user_id;
    private String status;
    private String priority;
    private String description;

    public Ticket() {
    }

    public Ticket(int id, int user_id, String status, String priority, String description) {
        this.id = id;
        this.user_id = user_id;
        this.status = status;
        this.priority = priority;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
