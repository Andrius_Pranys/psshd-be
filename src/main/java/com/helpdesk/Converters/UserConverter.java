package com.helpdesk.Converters;

import com.helpdesk.Dto.User;
import com.helpdesk.Entities.UserEntity;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class UserConverter {

    private TicketConverter ticketConverter;

    public UserConverter(TicketConverter ticketConverter) {
        this.ticketConverter = ticketConverter;
    }

    public UserEntity convert(User user) {
        if (user == null) {
            return null;
        }
        UserEntity result = new UserEntity();
        result.setId(user.getId());
        result.setEmail(user.getEmail());
        result.setName(user.getName());
        result.setAdmin(user.getAdmin());
        result.setTickets(ticketConverter.convertToEntityList(user.getTickets()));

        return result;
    }

    public User convert(UserEntity userEntity) {
        if (userEntity == null) {
            return null;
        }
        User result = new User();
        result.setId(userEntity.getId());
        result.setEmail(userEntity.getEmail());
        result.setName(userEntity.getName());
        result.setAdmin(userEntity.getAdmin());
        result.setTickets(ticketConverter.convert(userEntity.getTickets()));

        return result;
    }

    public List<User> convert(List<UserEntity> userEntities) {
        return userEntities.stream().map(this::convert).collect(toList());
    }
}
