package com.helpdesk.Converters;

import com.helpdesk.Dto.Ticket;
import com.helpdesk.Entities.TicketEntity;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class TicketConverter {

    public TicketEntity convert(Ticket ticket) {
        if (ticket == null) {
            return null;
        }

        TicketEntity result = new TicketEntity();
        result.setId(ticket.getId());
        result.setStatus(ticket.getStatus());
        result.setUser_id(ticket.getUser_id());
        result.setPriority(ticket.getPriority());
        result.setDescription(ticket.getDescription());

        return result;
    }

    public Ticket convert(TicketEntity ticketEntity) {

        if (ticketEntity != null) {

            Ticket result = new Ticket();
            result.setId(ticketEntity.getId());
            result.setStatus(ticketEntity.getStatus());
            result.setUser_id(ticketEntity.getUser_id());
            result.setPriority(ticketEntity.getPriority());
            result.setDescription(ticketEntity.getDescription());
            return result;
        }
        return null;
    }

    public List<Ticket> convert(List<TicketEntity> tickets) {
        if (tickets == null) {
            return null;
        }
        return tickets.stream().map(this::convert).collect(toList());
    }

    public List<TicketEntity> convertToEntityList(List<Ticket> tickets) {
        return tickets.stream().map(this::convert).collect(toList());
    }
}
