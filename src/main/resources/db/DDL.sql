create schema sda_final;

create table user
(
    id int unsigned auto_increment primary key,
    email varchar(250) not null unique,
    password varchar(60) not null,
    name varchar(50) not null,
    admin TINYINT(1) not null
);

create table ticket
(
    id int unsigned auto_increment primary key,
    user_id int unsigned,
    status varchar(16) not null,
    priority varchar(16) not null,
    description MEDIUMTEXT not null,
    foreign key (user_id) references user(id)
);
