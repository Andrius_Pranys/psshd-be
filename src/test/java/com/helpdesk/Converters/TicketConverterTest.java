package com.helpdesk.Converters;

import com.helpdesk.Dto.Ticket;
import com.helpdesk.Entities.TicketEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class TicketConverterTest {

    private TicketConverter ticketConverter;

    @BeforeEach
    void setUp() {
        ticketConverter = new TicketConverter();
    }

    @Test
    void convert() {
        List<TicketEntity> ticketEntityList = new ArrayList<>();
        List<Ticket> result = ticketConverter.convert(ticketEntityList);
        assertThat(result.isEmpty());
    }
}
